import Image from "next/image";
import spanish_flag from "@/public/images/spanish_flag.png";
import english_flag from "@/public/images/english_flag.png";

export const Nav = () => {
  return (
    <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light d-flex ">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-music"
            width="52"
            height="52"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="#00bfd8"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <circle cx="6" cy="17" r="3" />
            <circle cx="16" cy="17" r="3" />
            <polyline points="9 17 9 4 19 4 19 17" />
            <line x1="9" y1="8" x2="19" y2="8" />
          </svg>
          Music Theory App
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse flex-row-reverse"
          id="navbarNavDropdown"
        >
          <ul className="navbar-nav">
            {/*             <li className="nav-item m-2">
              <a
                className="nav-link underline d-flex justify-center align-items-center justify-content-center gap-2"
                href="#"
              >
                <Image src={spanish_flag} height={35} alt="spanish_flag" />
                Es
              </a>
            </li>
            <li className="nav-item m-2">
              <a
                className="nav-link underline d-flex justify-center align-items-center justify-content-center gap-2"
                href="#"
              >
                <Image src={english_flag} height={35} alt="english_flag" />
                En
              </a>
            </li> */}
            <li className="nav-item m-2">
              <a className="nav-link underline" href="#">
                Modo Mayor
              </a>
            </li>
            {/*
            <li className="nav-item m-2">
              <a className="nav-link underline" href="#minor-mode">
                Modo menor
              </a>
            </li>
            <li className="nav-item m-2">
              <a className="nav-link underline" href="#greeks-modes">
                Modos Griegos
              </a>
            </li>
            <li className="nav-item m-2"></li> */}
          </ul>
        </div>
      </div>
    </nav>
  );
};
