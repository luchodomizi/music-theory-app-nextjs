import React from "react";

export const Footer = () => {
  return (
    <footer>
      <div className="container d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
        <div className="col-md-4 d-flex align-items-center">
          <a
            href="#"
            className="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1 "
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="icon icon-tabler icon-tabler-music "
              width="52"
              height="52"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="#00bfd8"
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <path stroke="none" d="M0 0h24v24H0z" fill="none" />
              <circle cx="6" cy="17" r="3" />
              <circle cx="16" cy="17" r="3" />
              <polyline points="9 17 9 4 19 4 19 17" />
              <line x1="9" y1="8" x2="19" y2="8" />
            </svg>
          </a>
          <span className="text-muted copyright">© 2021 Humberto Domizi</span>
        </div>
        <ul className="nav col-md-4 justify-content-end list-unstyled d-flex">
          <li className="ms-3 footer-icon back-light">
            <a
              href="https://www.instagram.com/luchodomizi/"
              className="text-muted"
              target="_blank"
              rel="noopener noreferrer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-brand-instagram"
                width="40"
                height="40"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="#00bfd8"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                <rect x="4" y="4" width="16" height="16" rx="4" />
                <circle cx="12" cy="12" r="3" />
                <line x1="16.5" y1="7.5" x2="16.5" y2="7.501" />
              </svg>
            </a>
          </li>

          <li className="ms-3 footer-icon back-light">
            <a
              href="https://www.linkedin.com/in/humbertodomizi/"
              className="text-muted"
              target="_blank"
              rel="noopener noreferrer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-brand-linkedin"
                width="40"
                height="40"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="#00bfd8"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                <rect x="4" y="4" width="16" height="16" rx="2" />
                <line x1="8" y1="11" x2="8" y2="16" />
                <line x1="8" y1="8" x2="8" y2="8.01" />
                <line x1="12" y1="16" x2="12" y2="11" />
                <path d="M16 16v-3a2 2 0 0 0 -4 0" />
              </svg>
            </a>
          </li>
        </ul>
      </div>
    </footer>
  );
};
