import React from "react";

export const InfoMajorMode = () => {
  return (
    <div className="accordion mb-1" id="accordionExample">
      <div className="accordion-item">
        <h2 className="accordion-header" id="headingThree">
          <button
            className="accordion-button collapsed"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#collapseThree"
            aria-expanded="false"
            aria-controls="collapseThree"
          >
            ¿Cómo utilizar el MODO MAYOR?
          </button>
        </h2>
        <div
          id="collapseThree"
          className="accordion-collapse collapse"
          aria-labelledby="headingThree"
        >
          <div className="accordion-body">
            El <strong>Modo Mayor</strong> es una gran herramienta a la hora de
            componer canciones. Debemos saber que
            <strong> cada grado tiene su función</strong> dentro de la armonía
            de una canción, éstas funciones son 3:
            <ul className="my-3">
              <li>Tónicas</li>
              <li>Sub-Dominantes</li>
              <li>Dominantes</li>
            </ul>
            <hr />
            Las <strong>Tónicas primarias</strong> son aquellos grados que nos
            proveen de las bases de nuestra armonía y desde donde nace toda la
            estructura de lo que vamos a crear, funcionan como una especie de
            patrón desde donde vamos a sacar las escalas que pueden ser tocadas
            en dicha estructura. Cualquier
            <strong>Tónica</strong> dentro de su mismo modo (I, III y VI grado)
            puede ser reemplazada una con otra para usar variaciones armónicas.
            Por ejemplo <strong>Cmay</strong>, puede reemplazarse tanto de un
            <strong>Em</strong> como de un <strong>Am</strong>.
            <hr />
            Los <strong>Sub-Dominantes</strong> generan color en la armonía,
            solo están para dar orientaciones a la misma. En el modo mayor solo
            hay 2 grados que funcionan como <strong>Sub-Dominantes</strong>, son
            el II y IV grado, y también pueden reemplazarse entre si.
            <hr />
            Los <strong>Dominantes</strong> son grados que se utilizan
            generalmente como notas de paso hacia las Tónicas y son de extrema
            importancia a la hora de generar tensión armónica o{" "}
            <a
              href="https://es.wikipedia.org/wiki/Modulaci%C3%B3n_(m%C3%BAsica)"
              target="blank"
            >
              modular
            </a>
            . Los V y VII grados son Dominantes y también pueden reemplazarse el
            uno con el otro dentro de la armonía.
            <hr />
            <ul>
              <li>
                Los I, II, III, IV y VI grados, son acordes convencionales y
                para armarse cumplen con las reglas convencionales del armado de
                acordes: Acordes Mayores (Primera, Segunda, Tercera Justa,
                Cuarta, Quinta, Sexta Justa, Séptima Justa) y Acordes menores
                (Primera, Segunda, Tercera bemol, Cuarta, Quinta, Sexta bemol,
                Séptima bemol).
              </li>
              <li>
                Los acordes <strong>Dominantes</strong> merecen un{" "}
                <a
                  href="https://es.wikipedia.org/wiki/Dominante_(m%C3%BAsica)"
                  target="blank"
                >
                  apartado
                </a>{" "}
                para explicar su estructura ya que son acordes no
                convencionales.
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="accordion-item">
        <h2 className="accordion-header" id="headingTwo">
          <button
            className="accordion-button collapsed"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#collapseTwo"
            aria-expanded="false"
            aria-controls="collapseTwo"
          >
            ¿Cómo saber los géneros de cada grado en el Modo Mayor?
          </button>
        </h2>
        <div
          id="collapseTwo"
          className="accordion-collapse collapse"
          aria-labelledby="headingTwo"
        >
          <div className="accordion-body">
            <div className="my-2">
              Realmente es más simple de lo que parece:
            </div>
            <ul>
              <li>
                Si la distancia entre <strong>un grado</strong> y su{" "}
                <strong>tercer grado</strong>, es de 2 tonos o de una tercera
                mayor, el acorde será <strong>MAYOR</strong>
              </li>
              <div className="my-3 table-responsive col-lg-12">
                <table className="table table-light table-striped table-hover table-borderless">
                  <thead className="table-dark">
                    <tr>
                      <th scope="col">Primera</th>
                      <th scope="col">Tercera</th>
                      <th scope="col">Distancia</th>
                      <th scope="col">Género del acorde</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>C</td>
                      <td>E</td>
                      <td>2 Tonos (3a mayor)</td>
                      <td>Mayor</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <hr />
              <li>
                Si la distancia entre <strong>un grado</strong> y su{" "}
                <strong>tercer grado</strong>, es de 1 tono y medio o de una
                tercera menor, el acorde será <strong>menor</strong>
              </li>
              <div className="my-3 table-responsive col-lg-12">
                <table className="table table-light table-striped table-hover table-borderless">
                  <thead className="table-dark">
                    <tr>
                      <th scope="col">Primera</th>
                      <th scope="col">Tercera</th>
                      <th scope="col">Distancia</th>
                      <th scope="col">Género del acorde</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>C</td>
                      <td>D#</td>
                      <td>1 tono y medio (3a menor)</td>
                      <td>menor</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
