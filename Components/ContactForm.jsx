import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import axios from "axios";

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .min(5, "El nombre debe tener al menos 5 caracteres")
    .max(40, "El nombre no debe exceder los 40 caracteres")
    .required("El nombre es requerido"),
  email: Yup.string()
    .email("Dirección de correo inválida")
    .min(5, "El email debe tener al menos 5 caracteres")
    .max(40, "El email no debe exceder los 40 caracteres")
    .required("El email es requerido"),
  message: Yup.string()
    .min(5, "El mensaje debe tener al menos 5 caracteres")
    .max(1000, "El mensaje no debe exceder los 1000 caracteres")
    .required("El mensaje es requerido"),
});

const ContactForm = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [error, setError] = useState("");

  const handleSubmit = async (values, { resetForm }) => {
    setIsLoading(true);
    setIsSuccess(false);

    try {
      const response = await axios.post(
        "http://localhost:3001/send-email",
        values
      );
      setIsSuccess(true);
      setError("");
      setIsLoading(false);
      resetForm();
    } catch (error) {
      console.error("Error al enviar el email:", error.response.data);
      setError(error.response.data);
      setIsLoading(false);
    }
  };

  return (
    <div className="container mt-5">
      <h2>Contacta conmigo</h2>

      <Formik
        initialValues={{
          name: "",
          email: "",
          message: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isValid }) => (
          <Form>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Nombre
              </label>
              <Field
                type="text"
                className="form-control"
                id="name"
                name="name"
              />
              <ErrorMessage
                name="name"
                component="div"
                className="text-danger"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="email" className="form-label">
                Correo electrónico
              </label>
              <Field
                type="email"
                className="form-control"
                id="email"
                name="email"
              />
              <ErrorMessage
                name="email"
                component="div"
                className="text-danger"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="message" className="form-label">
                Mensaje
              </label>
              <Field
                as="textarea"
                className="form-control"
                id="message"
                name="message"
                rows="5"
              />
              <ErrorMessage
                name="message"
                component="div"
                className="text-danger"
              />
            </div>
            <button
              type="submit"
              className="btn btn-info mt-5"
              disabled={!isValid || isLoading}
            >
              {isLoading ? "Enviando..." : "Enviar"}
            </button>
            {isSuccess && <div style={{ color: "green" }}>Mensaje enviado</div>}
            {error && <div style={{ color: "red" }}>{error}</div>}
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default ContactForm;
