# Music Theory App Frontend - Hackaton SOHO 2023

Primer módulo de una aplicación de teoría musical para ayudar a músicos a componer canciones, brindando la escala y el grupo de acordes correspondientes a la nota elegida.

El proyecto está basado en éste https://musictheoryapp.netlify.app/ - https://gitlab.com/luchodomizi/music-theory-app creado en vanilla Javascript por mi hace unos años. Se basa en una clase constructora con sus métodos para formar tanto las escalas como los modos correspondientes al pasarles los parámetros adecuados.

El archivo **chord.class.js** , dentro de la carpeta helpers, es el motor del proyecto, fue optimizado utilizando ChatGPT en su versión 3.5, se tuvo que hacer algunos minimos cambios pero ha mejorado la integridad del código considerablemente.

**Como dato de color: hay un archivo no utilizado, llamado chordGPT4, en la carpeta helpers donde se mejora aún mas el código con GPT4 bajo el concepto de DRY (Don't Repeat Yourself).**

# Para levantar el proyecto seguir éstos pasos:

- git clone
- npm i
- npm run dev

Recursos:

- Node 18.16.0
- NextJS 13 (Javascript)
- Bootstrap 5.3.0

# Dejo los chats de ChatGPT donde se optimizó el código de la clase constructora:

- https://chat.openai.com/share/a994351a-8918-4bfe-999a-48fe5a17b6f8 GPT 3.5

- https://chat.openai.com/share/340197ed-4ced-46ff-9cdc-7a43b8550c12 GPT 4 para mejorar el código (no se utilizó este archivo en el proyecto ya que se mantuvo la integridad del prompt inicial)

# Para enviar mails de contacto se debe levantar el backend en **NodeJS** también:

- Repo del backend: https://gitlab.com/luchodomizi/music-theory-app-api-nodejs

Humberto Domizi - https://www.linkedin.com/in/humbertodomizi/
