export const notes = [
  "C",
  "Db",
  "D",
  "Eb",
  "E",
  "F",
  "Gb",
  "G",
  "Ab",
  "A",
  "Bb",
  "B",
];

const degreeToMode = {
  2: "min7",
  3: "min7",
  4: "maj7",
  5: "7",
  6: "min7",
  7: "min7(bemol5)",
};

export default class Chord {
  constructor(rootNote, type) {
    this.rootNote = rootNote;
    this.type = type;
    this.notes = [];
    this.chordsMode = [];
  }

  _getNotePosition(note) {
    const index = notes.indexOf(note);
    if (index === -1) {
      throw new Error(`Nota no válida: ${note}`);
    }
    return index;
  }

  _addDegree(degree, modeLabel) {
    const rootIndex = this._getNotePosition(this.rootNote);
    const noteIndex = (rootIndex + degree) % 12;
    this.notes.push(notes[noteIndex]);
    this.chordsMode.push(`${notes[noteIndex]}${modeLabel}`);
    return this;
  }

  getRoot() {
    return this._addDegree(0, "maj7");
  }

  addSecondDegree() {
    return this._addDegree(2, degreeToMode[2]);
  }

  addMinorThird() {
    return this._addDegree(3, degreeToMode[3]);
  }

  addMajorThird() {
    return this._addDegree(4, degreeToMode[4]);
  }

  addFourthDegree() {
    return this._addDegree(5, degreeToMode[4]);
  }

  addPerfectFifth() {
    return this._addDegree(7, degreeToMode[5]);
  }

  addSixthMajorDegree() {
    return this._addDegree(9, degreeToMode[6]);
  }

  addSixthMinorDegree() {
    return this._addDegree(8, degreeToMode[4]);
  }

  addMinorSeventh() {
    return this._addDegree(10, degreeToMode[7]);
  }

  addMajorSeventh() {
    return this._addDegree(11, degreeToMode[7]);
  }

  getNotes() {
    return {
      name: this.rootNote + this.type,
      structure: this.notes,
      mode: this.chordsMode,
    };
  }
}
