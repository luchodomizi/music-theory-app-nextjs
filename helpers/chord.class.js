export const notes = [
  "C",
  "Db",
  "D",
  "Eb",
  "E",
  "F",
  "Gb",
  "G",
  "Ab",
  "A",
  "Bb",
  "B",
];

export default class Chord {
  constructor(rootNote, type) {
    this.rootNote = rootNote;
    this.type = type;
    this.notes = [];
    this.chordsMode = [];
  }

  // Métodos privados para calcular la posición de una nota en el array de notas
  _getNotePosition(note) {
    const index = notes.indexOf(note);
    if (index === -1) {
      throw new Error(`Nota no válida: ${note}`);
    }
    return index;
  }

  getRoot() {
    this.notes.push(this.rootNote);
    this.chordsMode.push(`${this.rootNote}maj7`);
    return this;
  }

  addSecondDegree() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const secondIndex = (rootIndex + 2) % 12;
    this.notes.push(notes[secondIndex]);
    this.chordsMode.push(`${notes[secondIndex]}min7`);
    return this;
  }

  // Método para agregar la tercera menor del acorde
  addMinorThird() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const minorThirdIndex = (rootIndex + 3) % 12;
    this.notes.push(notes[minorThirdIndex]);
    this.chordsMode.push(`${notes[minorThirdIndex]}min7`);

    return this;
  }

  // Método para agregar la tercera mayor del acorde
  addMajorThird() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const majorThirdIndex = (rootIndex + 4) % 12;
    this.notes.push(notes[majorThirdIndex]);
    this.chordsMode.push(`${notes[majorThirdIndex]}maj7`);
    return this;
  }

  // Método para obtener el cuarto grado de la escala y agregarlo al array de notas
  addFourthDegree() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const fourthIndex = (rootIndex + 5) % 12;
    this.notes.push(notes[fourthIndex]);
    this.chordsMode.push(`${notes[fourthIndex]}maj7`);
    return this;
  }

  // Método para agregar la quinta justa del acorde
  addPerfectFifth() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const perfectFifthIndex = (rootIndex + 7) % 12;
    this.notes.push(notes[perfectFifthIndex]);
    this.chordsMode.push(`${notes[perfectFifthIndex]}7`);
    return this;
  }

  // Método para obtener la sexta mayor de la escala y agregarlo al array de notas
  addSixthMajorDegree() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const sixthIndex = (rootIndex + 9) % 12;
    this.notes.push(notes[sixthIndex]);
    this.chordsMode.push(`${notes[sixthIndex]}min7`);
    return this;
  }

  // Método para obtener la sexta menor de la escala y agregarlo al array de notas
  addSixthMinorDegree() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const sixthIndex = (rootIndex + 8) % 12;
    this.notes.push(notes[sixthIndex]);
    this.chordsMode.push(`${notes[sixthIndex]}maj7`);
    return this;
  }

  // Método para agregar la séptima menor a un acorde menor
  addMinorSeventh() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const seventhIndex = (rootIndex + 10) % 12;
    this.notes.push(notes[seventhIndex]);
    this.chordsMode.push(`${notes[seventhIndex]}min7(bemol5)`);
    return this;
  }

  // Método para agregar la séptima mayor a un acorde mayor
  addMajorSeventh() {
    const rootIndex = this._getNotePosition(this.rootNote);
    const seventhIndex = (rootIndex + 11) % 12;
    this.notes.push(notes[seventhIndex]);
    this.chordsMode.push(`${notes[seventhIndex]}min7(bemol5)`);
    return this;
  }

  getNotes() {
    return {
      name: this.rootNote + this.type,
      structure: this.notes,
      mode: this.chordsMode,
    };
  }
}
