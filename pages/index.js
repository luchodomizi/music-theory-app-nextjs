import { useEffect, useState } from "react";
import Script from "next/script";
import Head from "next/head";

import Chord, { notes } from "@/helpers/chord.class";

import { Nav } from "@/Components/Nav";
import { InfoMajorMode } from "@/Components/InfoMajorMode";
import { Footer } from "@/Components/Footer";

import axios from "axios";
import ContactForm from "@/Components/ContactForm";

export default function Home() {
  const [toneInfo, setToneInfo] = useState({});
  const [selectedNote, setSelectedNote] = useState("C");

  const getMajorTone = () => {
    const tone = new Chord(selectedNote, "maj")
      .getRoot()
      .addSecondDegree()
      .addMajorThird()
      .addFourthDegree()
      .addPerfectFifth()
      .addSixthMajorDegree()
      .addMajorSeventh()
      .getNotes();

    setToneInfo(tone);
  };

  return (
    <>
      <Head>
        <title>Music Theory App - NextJS 13</title>
      </Head>
      <main>
        <Script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
          crossOrigin="anonymous"
        />
        <Script src="//cdn.jsdelivr.net/npm/sweetalert2@11" />
        <Script
          src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.bundle.min.js"
          integrity="sha512-w3u9q/DeneCSwUDjhiMNibTRh/1i/gScBVp2imNVAMCt6cUHIw6xzhzcPFIaL3Q1EbI2l+nu17q2aLJJLo4ZYg=="
          crossOrigin="anonymous"
          referrerpolicy="no-referrer"
        />
        <Nav />

        <section className="container">
          <section id="mayor-mode">
            <div className="mt-3 col-lg-12 ">
              <form>
                <div className="text-center">
                  <label htmlFor="chords" className="chose_a_chord display-4">
                    Elija una nota
                  </label>
                </div>
                <div id="selectMayorChord">
                  <select
                    className="form-control form-control-lg text-center"
                    id="greeks_modes_select"
                    name="chords"
                    onChange={(e) => setSelectedNote(e.target.value)}
                  >
                    {notes.map((note, i) => (
                      <option key={i} value={note}>
                        {note}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="text-center">
                  <button
                    id="showMayorDeegres"
                    className="btn btn-info mt-5"
                    type="button"
                    onClick={getMajorTone}
                  >
                    Enviar
                  </button>
                </div>
              </form>
            </div>
            <div className="container" id="invoice">
              <div className="my-5 table-responsive col-lg-12">
                <h2 className="my-2 text-center">Escala Mayor</h2>
                <table className="table table-light table-striped table-hover table-borderless text-center">
                  <thead className="table-dark">
                    <tr>
                      <th scope="col">I</th>
                      <th scope="col">II</th>
                      <th scope="col">III</th>
                      <th scope="col">IV</th>
                      <th scope="col">V</th>
                      <th scope="col">VI</th>
                      <th scope="col">VII</th>
                    </tr>
                  </thead>

                  <tbody id="mayor_degrees">
                    <tr>
                      {toneInfo?.structure?.map((tone, i) => (
                        <td key={i}> {tone} </td>
                      ))}
                    </tr>
                  </tbody>
                </table>
              </div>

              <div className="mt-5 mb-3 table-responsive col-lg-12">
                <h2 className="my-3 text-center">
                  Modo Mayor - Acordes para componer
                </h2>
                <table className="table table-light table-striped table-hover table-borderless text-center">
                  <thead className="table-dark">
                    <tr>
                      <th scope="col">Primera</th>
                      <th scope="col">Segunda</th>
                      <th scope="col">Tercera</th>
                      <th scope="col">Cuarta</th>
                      <th scope="col">Quinta</th>
                      <th scope="col">Sexta</th>
                      <th scope="col">Séptima</th>
                    </tr>
                  </thead>
                  <tbody id="mayorMode">
                    <tr>
                      {toneInfo?.mode?.map((chord, i) => (
                        <td key={i}> {chord} </td>
                      ))}
                    </tr>
                  </tbody>
                  <tfoot className="table-info">
                    <tr>
                      <th>Tónica primaria</th>
                      <th>Sub-Dominante</th>
                      <th>Tónica</th>
                      <th>Sub-Dominante</th>
                      <th>Dominante</th>
                      <th>Tónica</th>
                      <th>Dominante</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>

            <InfoMajorMode />

            <ContactForm />
          </section>
        </section>

        <Footer />
      </main>
    </>
  );
}
